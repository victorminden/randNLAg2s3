__author__ = 'msingh'

import logging
import matplotlib.pyplot as plt
import numpy as np

logging.basicConfig(level=logging.INFO)
logger = logging.getLogger(__name__)


def rand_mul(mat, other):
    if mat.n != other.m:
        raise ValueError('Dimensions mismatch: Expected %d, instead got %d' %
                         (mat.n, other.m))
    sampling_probs = mat.sampling_probs()
    other_sampling_probs = other.sampling_probs(rows=False)
    den = sampling_probs.dot(other_sampling_probs)
    sampling_distributions = np.multiply(sampling_probs,
                                         other_sampling_probs) / den
    rows = np.random.choice(sampling_distributions, mat.c,
                            p=sampling_distributions)
    indices = [np.where(sampling_distributions == ele)[0][0] for ele in rows]
    rows = 1 / np.sqrt(rows * c)
    S = np.zeros((mat.n, mat.c))
    for index, s in enumerate(rows):
        this_index = indices[index]
        S[this_index][index] = s  # 1/sqrt(cp_j)

    C = mat.matrix.dot(S)
    R = S.transpose().dot(other.matrix)
    # Compute A.S.St.B
    return C.dot(R)


class RandNLA(object):

    def __init__(self, matrix, c):
        self.matrix = matrix
        self.m, self.n = matrix.shape
        self.c = c

    def shape(self):
        return self.m, self.n

    def sampling_probs(self, rows=True):
        axis = 0 if rows else 1
        norms = np.linalg.norm(self.matrix, axis=axis)
        return norms


def validate_bounds(Afro, Bfro, c, expect_err):
    rhs = (1 / np.sqrt(c)) * Afro * Bfro
    if expect_err <= rhs:
        logger.info('bounds validated %f < %f for %d', expect_err, rhs, c)
    else:
        logger.info('BOUNDS VIOLATED %f > %f for %d', expect_err, rhs, c)

def exp_with_c(A, B):
    c = [i for i in range(50)]
    AB = A.dot(B)
    true_ans = [np.linalg.norm(AB) for _ in range(len(c))]
    approx_ans = []
    for c_ in c:
        arandla = RandNLA(A, c_)
        brandla = RandNLA(B, c_)
        trials = 200
        ans = [np.linalg.norm(rand_mul(arandla, brandla))
                  for _ in range(trials)]
        approx_ans.append(np.mean(ans))
    plt.plot(c, true_ans, 'g--', label='A*B')
    plt.plot(c, approx_ans, 'b--', label='Rand A*B')
    plt.legend(loc='upper right')
    plt.xlabel('c')
    plt.ylabel('mat1*mat2')
    plt.savefig('rand_mul.png')
    #plt.show()




if __name__ == '__main__':
    A = np.random.randn(200, 50)
    B = np.random.randn(50, 100)
    c = 50
    arandla = RandNLA(A, c)
    brandla = RandNLA(B, c)
    AB = A.dot(B)
    errors = []
    trials = 200
    errors = [np.linalg.norm(AB - rand_mul(arandla, brandla), 'fro')
              for i in range(trials)]
    expect = np.mean(errors)
    logging.info('True ans: %f RandMul ans: %f' % (np.linalg.norm(AB), expect))
    logging.info('Rel error %f' % (expect / np.linalg.norm(AB)))
    validate_bounds(np.linalg.norm(A, 'fro'), np.linalg.norm(B, 'fro'), c,
                    expect)
    exp_with_c(A, B)